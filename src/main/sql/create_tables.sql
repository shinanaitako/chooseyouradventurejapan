DROP TABLE IF EXISTS monster CASCADE;

CREATE TABLE monster (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(128),
    image VARCHAR(512),
    description TEXT
);


DROP TABLE IF EXISTS actions CASCADE;

CREATE TABLE actions (
    id BIGSERIAL PRIMARY KEY,
    monster_id BIGINT,
    name VARCHAR(128),
    choice VARCHAR(128),
    description TEXT,
    result VARCHAR(128)
);