package ee.bcs.valiit.chooseyouradventurejapan.controller;

import ee.bcs.valiit.chooseyouradventurejapan.model.Monster;
import ee.bcs.valiit.chooseyouradventurejapan.model.MonsterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

@RestController
@RequestMapping("/")
public class MainController {

    private HashSet<Long> usedMonsters = new HashSet<>();

    private MonsterRepository monsterRepository;

    @Autowired
    public MainController(MonsterRepository monsterRepository) {
        this.monsterRepository = monsterRepository;
    }

    @GetMapping("/restart")
    public HashSet<Long> newOne() {
        usedMonsters = new HashSet<>();
        return usedMonsters;
    }

    @GetMapping("/monster")
    public Monster generateRandomMonster() {

        long monsterNumber = ThreadLocalRandom.current().nextLong(1, monsterRepository.findAll().size() + 1);

        Monster randomMonster = monsterRepository.getOne(monsterNumber);

        while (usedMonsters.add(monsterNumber) == false) {
            monsterNumber = ThreadLocalRandom.current().nextLong(1, monsterRepository.findAll().size() + 1);
            randomMonster = monsterRepository.getOne(monsterNumber);
        }
        usedMonsters.add(monsterNumber);
        return randomMonster;
    }
}
