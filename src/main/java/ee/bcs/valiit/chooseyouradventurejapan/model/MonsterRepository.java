package ee.bcs.valiit.chooseyouradventurejapan.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MonsterRepository extends JpaRepository<Monster, Long> {

    public List<Monster> findAll();

    public Optional<Monster> findMonsterByName(String name);
}


