package ee.bcs.valiit.chooseyouradventurejapan.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "monster")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Monster extends BaseEntity {

   @Column
   private String name;

   @Column
   private String image;

   @Column
   private String description;

   @JoinColumn(name = "monster_id")
   @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
   @Fetch(FetchMode.SELECT)
   public List<Action> actions;

}
