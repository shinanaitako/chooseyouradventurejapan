package ee.bcs.valiit.chooseyouradventurejapan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChooseyouradventurejapanApplication {


	public static void main(String[] args) {


		SpringApplication.run(ChooseyouradventurejapanApplication.class, args);
	}
}
