package ee.bcs.valiit.chooseyouradventurejapan.service;

import ee.bcs.valiit.chooseyouradventurejapan.model.Action;
import ee.bcs.valiit.chooseyouradventurejapan.model.Monster;
import ee.bcs.valiit.chooseyouradventurejapan.model.MonsterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@Component
@Service
public class DataLoaderService {

    private MonsterRepository monsterRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    public void dataLoaderRepository(MonsterRepository monsterRepository) {

        this.monsterRepository = monsterRepository;

        try {
            Resource res = resourceLoader.getResource("classpath:monsterfile.txt");
            Resource res2 = resourceLoader.getResource("classpath:easymonsterdescription.txt");
            List<String> monsterLines = Files.readAllLines(Paths.get(res.getURI()));
            List<String> actionLines = Files.readAllLines(Paths.get(res2.getURI()));

            for (String monsterLinePreSplit : monsterLines) {
                String[] monsterLine = monsterLinePreSplit.split(" & ");
                List<Action> actionList = new ArrayList<>();

                for (String actionLinesPreSplit : actionLines) {
                    String[] actionLine = actionLinesPreSplit.split(" & ");

                    if(actionLine[0].equals(monsterLine[0])) {
                        Action action = new Action(actionLine[0], actionLine[1], actionLine[2], actionLine[3], null);
                        actionList.add(action);
                    }
                }

                if (!monsterRepository.findMonsterByName(monsterLine[0]).isPresent()) {
                    Monster monster = new Monster(monsterLine[0], monsterLine[1], monsterLine[2], actionList);
                    this.monsterRepository.save(monster);
                }

            }
        } catch (IOException e) {
            System.out.println("Can't read importfile!");
        }
    }
}

